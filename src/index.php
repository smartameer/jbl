<?php
//checking if session exist from parent
if (!isset($_SESSION)) {
    session_set_cookie_params(0);
    session_start();
}

if ( !isset($_SESSION["jblreport"]) ) {
    $_SESSION['jblreport'] = array();
}

$config = [];
try {
    $configfile = "config.ini";
    $config = parse_ini_file(__DIR__ . "/" . $configfile, true);
} catch(Exception $e) {
    print "Error: Configuration not found.";
    die();
}
/**
 * cUrl for getting the data
 */
function resource($url, $params=array()) {
    $url = $url.'?'.http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

/**
 * for getting the token
 */
function get_token ($token_details=array(), $service) {
    global $_SESSION;
    if (isset($token_details['url'])) {
        $url = $token_details['url'];
        $params = array(
            "username" => $token_details['username'],
            "password" => $token_details['password'],
            "service" => $service
        );

        $token_response = resource($url, $params);
        $token_response = json_decode($token_response);
        $_SESSION['jblreport']['wstoken'] = $token_response->token;
    }
}

/**
 * for getting the courses list
 */
function get_courses($details=array(), $service) {
    global $_SESSION;
    if (isset($details['url'])) {
        $url = $details['url'];
        $params = array(
            "wstoken" => $_SESSION['jblreport']['wstoken'],
            "type" => "courselist",
        );
        $params = array_merge($params, $details['params']);
        $response = resource($url, $params);
        $response = str_replace(array("\"{", "}\"", "\\"), array("{", "}", ""), $response);
        $response = json_decode($response, true);
        return $response;
    }
}

//starting with course toiken directly
if (!isset($_SESSION['jblreport']['wstoken'])) {
    get_token($config['token'], $config['coursedetails']['service']);
}

// webservice request done to current url
if (isset($_POST) && count($_POST) > 0) {
    if (isset($_POST['course']) && is_numeric($_POST['course']) ) {
        $_SESSION['jblreport']['course'] = $_POST['course'];
        $_SESSION['jblreport']['users'] = '';
        if (isset($_POST['userids'])) {
            $users = [];
            foreach($_POST['userids'] as $id => $user) {
                $users[$id] = array('id' => $user, 'name' => $_POST['usernames'][$id]);
            }
            $_SESSION['jblreport']['users'] = json_encode($users);
        }
        header('Location: '.$_SERVER['REQUEST_URI']);
    } else {
        //bypassing webservice request to server
        if (isset($_POST['ajax']) && $_POST['ajax'] == true) {
            unset($_POST['ajax']);
            $url = $config['coursedetails']['url'];
            $params = array(
                "wstoken" => $_SESSION['jblreport']['wstoken'],
            );
            $params = array_merge($params, $_POST, $config['coursedetails']['params']);
            $response = resource($url, $params);
            $response = stripslashes($response);
            $response = str_replace(array("\"{", "}\""), array("{", "}"), $response);
            $response = stripslashes($response);
            header('Content-Type: application/json');
            print $response;
        } else {
            print "<h1 style='margin-left:40%;margin-top:20%;color:#d9534f;'>JBL Report: Course Not found. Try again later.</h1>";
        }
    }
} else {

    //showing form for selecting course
    if (isset($_SESSION['jblreport']['wstoken']) && !isset($_SESSION['jblreport']['course'])) {
        $courses = get_courses($config['coursedetails'], $config['coursedetails']['service']);
        print "<form action='' method='POST'>"
            ."<legend>Select Course</legend>"
            ."<select name='course'>";
        foreach($courses as $key => $course) {
            print "<option value='".$course['id']."'>".$course['name']."</option>";
        }
        print "</select><br/>"
            ."<input type='submit' value='Submit' />"
            ."</form>";
        exit;
    }

    //showing report if token fetched
    if (isset($_SESSION['jblreport']['wstoken']) && isset($_SESSION['jblreport']['course'])) {
        if (isset($config['baseurl'])) {
            print "<script>var jbldetails = { 'baseurl': '".$config['baseurl']."', 'course': ".$_SESSION['jblreport']['course']." , 'users': ".$_SESSION['jblreport']['users']."};</script>";
            @include_once __DIR__.'/jblreport.html';
        } else {
            print "<h1 style='margin-left:40%;margin-top:20%;color:#d9534f;'>JBL Report: Configuartion Error. Try again later.</h1>";
        }
    }

}
