(function(){
    'use strict';

    angular.module('jbl')
        .factory('HttpInterceptor', HttpInterceptor);

    function HttpInterceptor($q) {
        return {
            request : function(config) {
                config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
                config.transformRequest = function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                };
                return config;
            },

            response: function(response) {
                return response || $q.when(response);
            },

            requestError: function(rejection) {
                return $q.reject(rejection);
            },

            responseError: function(rejection) {
                return $q.reject(rejection);
            }
        };
    }
})();
