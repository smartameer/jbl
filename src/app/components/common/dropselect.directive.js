/**
 * @description: Dropdown Selection
 * @author: Pradeep Patro <patro.pradeep@outlook.com>
 */

/* global angular */
(function() {
    'use strict';

    angular
        .module('jbl')
        .directive('dropSelect', DropSelect);

    function DropSelect () {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                type: '@',
                placeholder: '@',
                model: '=',
                options: '='
            },
            templateUrl: 'app/components/common/dropselect.html',
            controller: DropSelectController,
            controllerAs: 'dsc',
            bindToController: true
        };
    }

    function DropSelectController ($scope, $timeout, $log) {
        var vm = this;
        vm.strarr = [];
        vm.value = null;
        vm.open = false;
        vm.openDropdown = function () {
            vm.open = !vm.open;
        };

        vm.generateStr = function() {
            vm.strarr = [];
            $timeout(function() {
                angular.forEach(vm.options, function(item) {
                    if (item.selected === true) {
                        vm.strarr.push(item.name);
                    }
                });
                vm.value = (vm.strarr.length > 0) ? vm.strarr.join(', ').substr(0, 20) + ' ...' : null;
            }, 200);
        };

        vm.selectAll = function () {
            vm.model = [];
            angular.forEach(vm.options, function (item, index) {
                vm.options[index].selected = true;
                vm.model.push(item);
            });
            vm.generateStr();
        };

        vm.deselectAll = function () {
            vm.model = [];
            angular.forEach(vm.options, function (item, index) {
                vm.options[index].selected = false;
            });
            vm.value = null;
        };

        vm.toggleSelect = function () {
            vm.model = [];
            $timeout(function() {
                angular.forEach(vm.options, function (item) {
                    if (item.selected === true) {
                        vm.model.push(item);
                    }
                });
            }, 10);
            vm.generateStr();
        };

        var timer = null;
        $scope.$on('report:dataready', function(event, args) {
            if (timer !== null) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function() {
                if (args.type === vm.type) {
                    $log.info("Selecting all "+vm.type);
                    vm.selectAll();
                }
            }, 1000);
        });
        $scope.$on('$destroy', function() {
            $timeout.cancel(timer);
        });
    }
})();
