(function() {
  'use strict';

  angular
    .module('jbl')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) { //, $locationProvider) {
      //$locationProvider.html5Mode(true);
      $stateProvider
          .state('home', {
              url: '/',
              templateUrl: 'app/report/report.html',
              controller: 'ReportController',
              controllerAs: 'rc'
          });

      $urlRouterProvider.otherwise('/');
  }

})();
