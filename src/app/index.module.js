(function() {
  'use strict';

  angular
    .module('jbl', ['ui.router', 'ui.bootstrap', 'ui.select', 'toastr']);

})();
