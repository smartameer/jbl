/* global jbldetails */
(function() {
  'use strict';

  angular
    .module('jbl')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, JBL_CONSTANTS) {
      if (angular.isDefined(jbldetails.baseurl)) {
          JBL_CONSTANTS.BASE_URI = jbldetails.baseurl;
      }
      if (angular.isDefined(jbldetails.course)) {
          JBL_CONSTANTS.COURSEID = jbldetails.course;
      }
      if (angular.isDefined(jbldetails.users)) {
          JBL_CONSTANTS.USERS = jbldetails.users;
      }
      $log.debug('runBlock end');
  }

})();
