(function() {
  'use strict';

  angular
    .module('jbl')
    .config(config);

  /** @ngInject */
  function config($logProvider, $httpProvider, toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);

    $httpProvider.interceptors.push('HttpInterceptor');
    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 5000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.newestOnTop = true;
    toastrConfig.maxOpened = 5;
    toastrConfig.preventOpenDuplicates = true;
    toastrConfig.progressBar = true;
    toastrConfig.tapToDismiss = true;
    toastrConfig.closeButton = true;
  }

})();
