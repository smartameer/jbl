(function() {
  'use strict';

  angular
    .module('jbl')
    .constant('JBL_CONSTANTS', {
        BASE_URI: '/',
        BASE_PARAMS: {
            ajax: true,
            wsfunction: 'graph',
            moodlewsrestformat: 'json',
            courseVal: '',
            gradelabel: '',
            gradeVal: '',
            sectionlabel: '',
            sectionVal: '',
            sectiontype: '',
            assetlabel: '',
            assetval: '',
            activitylable: '',
            studentlabel: '',
            studentVal: ''
        },
        COURSEID: 0,
        USERS: {},
        PARAMS: {
            CHAPTERS: {
                type: 'chapterlist'
            },
            TOPICS: {
                type: 'topiclist'
            },
            ACTIVITY: {
                type: 'activity'
            },
            ASSETS: {
                type: 'assetlist'
            },
            REPORT: {
                type: 'report1'
            }
        }
    });

})();
