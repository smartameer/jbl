/* global angular, c3, d3 */
(function() {
  'use strict';

  angular
    .module('jbl')
    .controller('ReportController', ReportController);

  /** @ngInject */
  function ReportController($scope, $document, $timeout, $filter, $log, $http, toastr, JBL_CONSTANTS) {
      var vm = this;
      vm.collapsed = false;
      vm.selectionscollapsed = true;
      vm.showGraph = false;
      vm.showTable = false;
      vm.graphDataReady = false;
      vm.showNoDataError = null;
      vm.getColorCodes = function (graph) {
          var colors = {
              'grade': '#004c8f',
              'quiz': '#00b38a',
              'navquiz': '#9abb7e',
              'ebookquiz': '#5b294b',
              'midtermexam': '#7dabbf',
              'ebook': '#e7a643',
              'finalexam': '#c0562e',
              'average': '#d51d29'
          },
          arr = {};

          if (angular.isString(graph)) {
              return colors[graph.toLowerCase()];
          }

          angular.forEach(graph, function(data) {
              if (data[0] !== 'x') {
                  var column = (data[0] === 'Grade') ? data[0] : $filter('filter')(vm.engagements, {name: data[0]}, true)[0].id;
                  arr[data[0]] = colors[column.toLowerCase()];
              }
          });
          return arr;
      };

      vm.sortWithIndeces = function (toSort) {
          for (var i = 0; i < toSort.length; i++) {
              toSort[i] = [toSort[i], i];
          }
          toSort.sort(function(left, right) {
              if (parseFloat(left[0]) < parseFloat(right[0])) {
                  return 1;
              }
              if (parseFloat(left[0]) > parseFloat(right[0])) {
                  return -1;
              }
              return 0;
          });
          toSort.sortIndices = [];
          for (var j = 0; j < toSort.length; j++) {
              toSort.sortIndices.push(toSort[j][1]);
              toSort[j] = toSort[j][0];
          }
          return toSort;
      };

      vm.toggledIds = {
          x: true,
          Grade: false
      };
      vm.stepsStr = {
          grade: 'By Grade',
          engagement: 'By Engagement',
          gradeengagement: 'By Grade & Engagement',
          chapter: 'By Chapter',
          topic: 'By Topic',
          assessment: 'By Assessment',
          top: 'Top',
          bottom: 'Bottom',
          all: 'All'
      };

      vm.selected = {
          courses: [{id: JBL_CONSTANTS.COURSEID}],
          engagements: null,
          topics: null,
          chapters: null,
          assessments: null,
          users: null
      };

      //vm.timespendFormat = function(val) {
      //    return Math.round(val/60);
      //};

      vm.by = {
          step1: null,
          step2: null,
          step3: null,
          step4: null
      };

      vm.engagements = [];
      vm.topics = [];
      vm.chapters = [];
      vm.assessments = [];
      vm.users = angular.copy(JBL_CONSTANTS.USERS);
      vm.percent = {
          top: 10,
          bottom: 10
      };

      vm.toArray = function(data) {
          var d = angular.fromJson(data);
          return Object.keys(d).map(function(k) {
              return d[k];
          });
      };


      vm.getActivities = function () {
          var params = angular.copy(JBL_CONSTANTS.BASE_PARAMS);
          if (vm.selected.courses !== null) {
              var clist = [];
              angular.forEach(vm.selected.courses, function(c) {
                  clist.push(c.id);
              });
              params.courseVal = '[' +clist.join(',') + ']';
          }
          angular.extend(params, JBL_CONSTANTS.PARAMS.ACTIVITY);
          $http({
              url: JBL_CONSTANTS.BASE_URI,
              method: 'POST',
              cache: true,
              data: params
          }).then(function(response) {
              vm.engagements = vm.toArray(response.data);
              $scope.$broadcast('report:dataready', {type: 'engagements'});
              $timeout(function() {
                  $scope.$broadcast('report:dataready', {type: 'users'});
              }, 1000);
          }, function() {
              $log.debug('Request Failed');
          });
      };

      vm.getChapters = function() {
          vm.selected.chapters = null;
          vm.selected.topics = null;
          vm.selected.assessments = null;
          vm.by.step3 = null;
          vm.by.step4 = 'all';
          var params = angular.copy(JBL_CONSTANTS.BASE_PARAMS);
          if (vm.selected.courses !== null) {
              var clist = [];
              angular.forEach(vm.selected.courses, function(c) {
                  clist.push(c.id);
              });
              params.courseVal = '[' +clist.join(',') + ']';
          }
          angular.extend(params, JBL_CONSTANTS.PARAMS.CHAPTERS);
          if (vm.by.step1 !== null) {
              if (vm.by.step1 === 'gradeengagement') {
                  params.gradelabel = 'all';
                  params.gradeVal = 'all';
              } else {
                  params.gradelabel = vm.by.step1;
                  params.gradeVal = vm.by.step1;
              }

              if (vm.by.step1 === 'engagement' || vm.by.step1 === 'gradeengagement') {
                  if (vm.selected.engagements !== null) {
                      var elist = [];
                      angular.forEach(vm.selected.engagements, function(e) {
                          elist.push(e.id);
                      });
                      if (elist.length <= 0) {
                          toastr.warning('Please select atleast an engagement');
                          return;
                      }
                      params.activitylable = elist.join(',');
                  } else {
                      toastr.warning('Please select atleast an engagement');
                      return;
                  }
              }
          } else {
              toastr.warning('Complete Step 1 by selecting either By Grade or By Engagements or By Grade & Engagements.');
              return;
          }

          $http({
              url: JBL_CONSTANTS.BASE_URI,
              method: 'POST',
              cache: true,
              data: params
          }).then(function(response) {
              vm.chapters = vm.toArray(response.data);
              angular.forEach(vm.chapters, function(data) {
                  data.name = data.name.replace('CH_', 'Chapter ');
              });
              $scope.$broadcast('report:dataready', {type: 'chapters'});
          }, function() {
              $log.debug('Request Failed');
          });
      };

      vm.getTopics = function() {
          vm.selected.chapters = null;
          vm.selected.topics = null;
          vm.selected.assessments = null;
          vm.by.step3 = null;
          vm.by.step4 = 'all';
          var params = angular.copy(JBL_CONSTANTS.BASE_PARAMS);
          angular.extend(params, JBL_CONSTANTS.PARAMS.TOPICS);
          if (vm.selected.courses !== null) {
              var clist = [];
              angular.forEach(vm.selected.courses, function(c) {
                  clist.push(c.id);
              });
              params.courseVal = '[' +clist.join(',') + ']';
          }

          $http({
              url: JBL_CONSTANTS.BASE_URI,
              method: 'POST',
              cache: true,
              data: params
          }).then(function(response) {
              vm.topics = vm.toArray(response.data);
              $scope.$broadcast('report:dataready', {type: 'topics'});
          }, function() {
              $log.debug('Request Failed');
          });
      };

      vm.getAssesments = function () {
          var params = angular.copy(JBL_CONSTANTS.BASE_PARAMS);
          params = angular.extend(params, JBL_CONSTANTS.PARAMS.ASSETS);

          var courseslist = []
          angular.forEach(vm.selected.courses, function(data) {
              courseslist.push(data.id);
          });
          params.courseVal = '[' +courseslist.join(',') + ']';

          if (vm.by.step1 !== null) {
              if (vm.by.step1 === 'gradeengagement') {
                  params.gradelabel = 'all';
                  params.gradeVal = 'all';
              } else {
                  params.gradelabel = vm.by.step1;
                  params.gradeVal = vm.by.step1;
              }

              if (vm.by.step1 === 'engagement' || vm.by.step1 === 'gradeengagement') {
                  if (vm.selected.engagements !== null) {
                      var elist = [];
                      angular.forEach(vm.selected.engagements, function(e) {
                          elist.push(e.id);
                      });
                      if (elist.length <= 0) {
                          toastr.warning('Please select atleast an engagement');
                          return;
                      }
                      params.activitylable = elist.join(',');
                  } else {
                      toastr.warning('Please select atleast an engagement');
                      return;
                  }
              }
          } else {
              toastr.warning('Complete Step 1 by selecting either By Grade or By Engagements or By Grade & Engagements.');
              return;
          }

          if (vm.by.step2 === null) {
              toastr.warning('Complete Step 2 by selecting the option "By Chapters".');
          } else {
              if (vm.selected[vm.by.step2 + 's'] !== null) {
                  var clist = [], llist = [], key = vm.by.step2 + 's';
                  angular.forEach(vm.selected[key], function(c) {
                      clist.push(c.id);
                      var name = (vm.by.step2 === 'chapter') ? c.name.replace('Chapter ', 'CH_') : c.name;
                      llist.push('"'+name+'"');
                  });
                  if (clist.length <= 0) {
                      vm.by.step3 = null;
                      toastr.warning('Select some '+key+' to get assessments list.');
                      return;
                  }
                  params.sectionVal = '[' +clist.join(',') + ']';
                  params.sectionlabel = '[' +llist.join(',') + ']';
                  params.sectiontype = vm.by.step2;
              } else {
                  toastr.warning('Something went wrong. Please try again by refreshing.');
              }
          }

          $http({
              url: JBL_CONSTANTS.BASE_URI,
              method: 'POST',
              cache: true,
              data: params
          }).then(function(response) {
              vm.assessments = vm.toArray(response.data);
              $scope.$broadcast('report:dataready', {type: 'assessments'});
          }, function() {
              $log.debug('Request Failed');
          });
      };

      vm.showReport = function() {
          vm.showGraph = false;
          vm.showTable = false;
          vm.graphDataReady = false;
          vm.showNoDataError = null;
          if (angular.isDefined(vm.graphReport)) {
              vm.graphReport.unload();
          }
          var params = angular.copy(JBL_CONSTANTS.BASE_PARAMS);
          params = angular.extend(params, JBL_CONSTANTS.PARAMS.REPORT);

          if (vm.selected.courses !== null) {
              var courselist = [];
              angular.forEach(vm.selected.courses, function(c) {
                  courselist.push(c.id);
              });
              if (courselist.length <= 0) {
                  toastr.warning('<strong>Warning !!!</strong><br/>Please select atleast a course');
                  return;
              }
              params.courseVal = '[' +courselist.join(',') + ']';
          } else {
              alert('Select atleast a course');
              return;
          }

          if (vm.by.step1 !== null) {
              if (vm.by.step1 === 'gradeengagement') {
                  params.gradelabel = 'all';
                  params.gradeVal = 'all';
              } else {
                  params.gradelabel = vm.by.step1;
                  params.gradeVal = vm.by.step1;
              }

              if (vm.by.step1 === 'engagement' || vm.by.step1 === 'gradeengagement') {
                  if (vm.selected.engagements !== null) {
                      var elist = [];
                      angular.forEach(vm.selected.engagements, function(e) {
                          elist.push(e.id);
                      });
                      if (elist.length <= 0) {
                          toastr.warning('Please select atleast an engagement');
                          return;
                      }
                      params.activitylable = elist.join(',');
                  } else {
                      toastr.warning('Please select atleast an engagement');
                      return;
                  }
              }
          } else {
              toastr.warning('Complete Step 1 by selecting either By Grade or By Engagements or By Grade & Engagements.');
              return;
          }

          if (vm.by.step2 === null) {
              toastr.warning('Complete Step 2 by selecting the option "By Chapters".');
              return;
          } else {
              if (vm.selected[vm.by.step2 + 's'] !== null) {
                  var clist = [],
                      llist = [],
                      key = vm.by.step2 + 's';
                  angular.forEach(vm.selected[key], function(c) {
                      clist.push(c.id);
                      var name = (vm.by.step2 === 'chapter') ? c.name.replace('Chapter ', 'CH_') : c.name;
                      llist.push('"'+name+'"');
                  });
                  if (clist.length <= 0) {
                      vm.by.step3 = null;
                      toastr.warning('Select some '+key+' to get graph.');
                      return;
                  }
                  params.sectionVal = '[' +clist.join(',') + ']';
                  params.sectionlabel = '[' +llist.join(',') + ']';
                  params.sectiontype = vm.by.step2;
              } else {
                  toastr.warning('Something went wrong. Please try again by refreshing.');
              }
          }

          if (vm.by.step3 === 'assessment' && vm.selected.assessments === null) {
              toastr.warning('Select some assessments.');
              return;
          } else if (vm.by.step3 !== null) {
              if (vm.selected.assessments !== null) {
                  var alist = [], allist = [];
                  angular.forEach(vm.selected.assessments, function(c) {
                      alist.push(c.id);
                      allist.push(c.name);
                  });
                  if (alist.length <= 0) {
                      vm.by.step3 = null;
                      toastr.warning('Select some assessments to get graph.');
                      return;
                  }
                  params.assetval = '[' +alist.join(',') + ']';
                  params.assetlabel = '[' +allist.join(',') + ']';
              } else {
                  toastr.warning('Something went wrong. Please try again by refreshing.');
                  return;
              }
          }

          if (vm.by.step4 !== null && vm.by.step4 !== 'all') {
              params.levelval = parseInt(vm.percent[vm.by.step4]);
              if (isNaN(params.levelval) || angular.isUndefined(params.levelval)) {
                  toastr.warning('Wrong value entered for '+vm.by.step4+' %');
                  return;
              } else if (params.levelval < 0 || params.levelval > 100) {
                  toastr.warning('Choose between 1 to 100 percent');
                  return;
              }
              params.leveltype = vm.by.step4;
          } else {
              if (vm.selected.users !== null) {
                  var ulist = [];
                  angular.forEach(vm.selected.users, function(c) {
                      ulist.push(c.id);
                  });
                  if (ulist.length <= 0) {
                      toastr.warning('Select some users to get graph.');
                      return;
                  }
                  params.studentVal = '[' +ulist.join(',') + ']';
              }
          }

          $http({
              url: JBL_CONSTANTS.BASE_URI,
              method: 'POST',
              data: params
          }).then(function(response) {
              var report = response.data;
              if (report.total > 0 && (report.data !== null && vm.toArray(report.data).length > 0)) {
                  vm.createGraph(vm.toArray(report.data));
                  vm.collapsed = true;
                  vm.showNoDataError = null;
              } else {
                  vm.showNoDataError = true;
                  toastr.info('No records found.');
                  return;
              }
          }, function() {
              $log.debug('Request Failed');
          });
      };

      vm.createGraph = function(data) {
          if (vm.by.step1 === 'grade') {
              vm.createGradeGraph(data);
          } else if (vm.by.step1 === 'engagement') {
              vm.createEngagementGraph(data);
          } else if (vm.by.step1 === 'gradeengagement') {
              vm.createGradeEngagementGraph(data);
          }

          var records = {};
          var headers = {};
          var chaptername = '';
          var assessmentname = '';
          angular.forEach(data, function(sdata) {
              records[sdata.studentName] = {};
              angular.forEach(sdata.grade, function(gdata) {
                  chaptername = gdata.chapter;
                  if (chaptername === 0) {
                      chaptername = 'Chapter Other';
                  } else {
                      try {
                          chaptername = $filter('filter')(vm.chapters, {id: gdata.chapter}, true)[0].name;
                      } catch (e) {
                          $log.error('No such chapter exists for id : ' + gdata.chapter);
                      }
                  }
                  if (angular.isUndefined(records[sdata.studentName][chaptername])) {
                      records[sdata.studentName][chaptername] = {};
                      if (angular.isUndefined(headers[chaptername])) {
                          headers[chaptername] = {};
                      }
                  }
                  if (vm.by.step3 === 'assessment') {    // if step3 assessment is selected
                      assessmentname = gdata.cmid;
                      try {
                          assessmentname = $filter('filter')(vm.assessments, {id: gdata.cmid}, true)[0].name;
                      } catch (e) {
                          $log.error('No such assessment exists for id : ' + gdata.chapter);
                      }
                      if (angular.isUndefined(records[sdata.studentName][chaptername][assessmentname])) {
                          records[sdata.studentName][chaptername][assessmentname] = {};
                          headers[chaptername][assessmentname] = {};
                          if (vm.by.step1 === 'grade' || vm.by.step1 === 'gradeengagement') {
                              headers[chaptername][assessmentname]['grade'] = true;
                          }
                          if (vm.by.step1 === 'engagement' || vm.by.step1 === 'gradeengagement') {
                              headers[chaptername][assessmentname]['engagement'] = true;
                          }
                      }

                      if (vm.by.step1 === 'grade' || vm.by.step1 === 'gradeengagement') {
                          if (angular.isUndefined(records[sdata.studentName][chaptername][assessmentname]['grade'])) {
                              records[sdata.studentName][chaptername][assessmentname]['grade'] = 0;
                              records[sdata.studentName][chaptername][assessmentname]['gradeCount'] = 0;
                          }
                          if (parseFloat(gdata.grade) > 0) {
                              records[sdata.studentName][chaptername][assessmentname].grade += parseFloat(gdata.grade);
                              records[sdata.studentName][chaptername][assessmentname].gradeCount ++;
                          }
                      }

                      if (vm.by.step1 === 'engagement' || vm.by.step1 === 'gradeengagement') {
                          if (angular.isUndefined(records[sdata.studentName][chaptername][assessmentname]['engagement'])) {
                              records[sdata.studentName][chaptername][assessmentname]['engagement'] = 0;
                          }
                          records[sdata.studentName][chaptername][assessmentname].engagement += parseInt(gdata.timespend);
                      }
                  } else {   // till step2 only
                      if (vm.by.step1 === 'grade' || vm.by.step1 === 'gradeengagement') {
                          if (angular.isUndefined(records[sdata.studentName][chaptername]['grade'])) {
                              records[sdata.studentName][chaptername]['grade'] = {};
                              headers[chaptername]['grade'] = true;
                          }
                          if (angular.isUndefined(records[sdata.studentName][chaptername]['grade'][gdata.assessment])) {
                              records[sdata.studentName][chaptername]['grade'][gdata.assessment] = {'grade' : 0, 'gradeCount': 0};
                          }
                          if (parseFloat(gdata.grade) > 0) {
                              records[sdata.studentName][chaptername]['grade'][gdata.assessment].grade += parseFloat(gdata.grade);
                              records[sdata.studentName][chaptername]['grade'][gdata.assessment].gradeCount ++;
                          }
                      }

                      if (vm.by.step1 === 'engagement' || vm.by.step1 === 'gradeengagement') {
                          if (angular.isUndefined(records[sdata.studentName][chaptername]['engagement'])) {
                              records[sdata.studentName][chaptername]['engagement'] = 0;
                              headers[chaptername]['engagement'] = true;
                          }

                          if (gdata.quizType !== 'ebook') {
                              records[sdata.studentName][chaptername].engagement += parseInt(gdata.timespend);
                          }
                      }

                      if (gdata.quizType === 'ebook') {
                          if (angular.isUndefined(records[sdata.studentName][chaptername]['ebook'])) {
                              records[sdata.studentName][chaptername]['ebook'] = 0;
                              headers[chaptername]['ebook'] = true;
                          }
                          records[sdata.studentName][chaptername].ebook += parseInt(gdata.timespend);
                      }
                  }
              });
          });

          vm.tableHeaders = {
              chapters: [],
              assessments: [],
              studentHeaders: [],
              values: {}
          };
          var index = 0,
              studentHeadersKeys;

          headers = vm.orderKeys(headers);
          angular.forEach(headers, function(data, key) {
              vm.tableHeaders.chapters[index] = {
                  key: key,
                  span: vm.objlen(data)
              };
              if (vm.by.step3 === 'assessment') {
                  var assessmentsSpan = 0;
                  angular.forEach(data, function (a, k) {
                      assessmentsSpan = vm.objlen(a);
                      vm.tableHeaders.assessments.push({
                          key : k,
                          span: vm.objlen(a)
                      });

                      studentHeadersKeys = Object.keys(a);
                      studentHeadersKeys[studentHeadersKeys.indexOf('engagement')] = 'Time Spent';
                      studentHeadersKeys[studentHeadersKeys.indexOf('grade')] = 'Grade';
                      vm.tableHeaders.studentHeaders = vm.tableHeaders.studentHeaders.concat(studentHeadersKeys);
                      angular.forEach(a, function(val, type) {
                          angular.forEach(records, function(d, student) {
                              var value = 0;
                              if (angular.isUndefined(vm.tableHeaders.values[student])) {
                                  vm.tableHeaders.values[student] = [];
                              }
                              try {
                                  if (type === 'grade' && angular.isDefined(d[key][k].gradeCount) && d[key][k].gradeCount > 1) {
                                      value = d[key][k][type] / d[key][k].gradeCount;
                                  } else {
                                      value = d[key][k][type];
                                  }
                              } catch(e) {
                                  $log.debug('No course Record for '+key+' course for student :'+ student);
                              }
                              if (type === 'grade') {
                                  value = Math.round(value);
                              //} else {
                              //    value = Math.round(value / 60);
                              }
                              vm.tableHeaders.values[student].push(value);
                          });
                      });
                  });
                  vm.tableHeaders.chapters[index].span = vm.tableHeaders.chapters[index].span * assessmentsSpan;
              } else {
                  studentHeadersKeys = Object.keys(data);
                  studentHeadersKeys[studentHeadersKeys.indexOf('engagement')] = 'Time Spent';
                  studentHeadersKeys[studentHeadersKeys.indexOf('grade')] = 'Grade';
                  studentHeadersKeys[studentHeadersKeys.indexOf('ebook')] = 'Ebook Time Spent';
                  vm.tableHeaders.studentHeaders = vm.tableHeaders.studentHeaders.concat(studentHeadersKeys);

                  angular.forEach(data, function(val, type) {
                      angular.forEach(records, function(d, student) {
                          var mark = 0;
                          if (angular.isUndefined(vm.tableHeaders.values[student])) {
                              vm.tableHeaders.values[student] = [];
                          }
                          if (angular.isDefined(d[key])) {
                              try {
                                  mark = d[key][type];
                              } catch(e) {
                                  $log.debug('No course Record for '+key+' course for student :'+ student);
                              }
                          }
                          if (angular.isObject(mark)) {
                              var grade_val = {};
                              angular.forEach(mark, function (m, i) {
                                  if (m.gradeCount > 0) {
                                      grade_val[i] = m.grade / m.gradeCount;
                                  } else {
                                      grade_val[i] = m.grade;
                                  }
                              });
                              var grade_count = 0;
                              var grade_data = 0;
                              angular.forEach(grade_val, function (g) {
                                  if (parseFloat(g) > 0 ) {
                                      grade_data += parseFloat(g);
                                      grade_count += 1;
                                  }
                              })
                              if (grade_count > 0) {
                                  mark = grade_data/grade_count;
                              } else {
                                  mark = 0;
                              }
                          }

                          if (type === 'grade') {
                              mark = Math.floor(mark);
                          //} else {
                          //    mark = Math.round(mark / 60);
                          }

                          vm.tableHeaders.values[student].push(mark);
                      });
                  });
              }
              index++;
          });
      };

      /**
       * @description : Grade Graph
       */
      vm.createGradeGraph = function(data) {
          vm.getGraphRecords(data, { timespend: false, grade: true, sort: true }, function (graph) {
              $timeout(function() {
                  vm.showGraph = true;
                  vm.showTable = false;
                  vm.graphDataReady = true;
                  vm.graphReport = c3.generate({
                      bindto: '#jbl-graph-report',
                      data: {
                          x: 'x',
                          columns: graph,
                          type: 'bar',
                          //labels: true,
                          colors: (function(){
                              return vm.getColorCodes(graph);
                          })()
                      },
                      legend: {
                          show: false
                      },
                      axis : {
                          rotated: true,
                          x: {
                              type: 'category',
                              label: { text: 'Student Name', position: 'outer-top' }
                          },
                          y: {
                              label: { text: 'Grade', position: 'outer-center' }
                          }
                      },
                      bar: {
                          width: 20
                      },
                      grid: {y: { show: true } }
                  });
                  $timeout(function() {
                      vm.graphReport.resize({height: 100 * vm.selected.users.length});
                      vm.createGraphControls(graph, vm.graphReport, {
                          grade: {
                              axis: 'y'
                          }
                      });
                  }, 10);
              }, 0);
           });
      };

      /**
       * @description : Engagement Graph
       */
      vm.createEngagementGraph = function(data) {
          vm.getGraphRecords(data, {timespend: true, grade: false, sort: false}, function(graph) {
              var activityNames = [];
              angular.forEach(vm.engagements, function(en) {
                  if (en.selected === true) {
                      activityNames.push(en.name);
                  }
              });

              $timeout(function() {
                  vm.showGraph = true;
                  vm.showTable = false;
                  vm.graphDataReady = true;
                  vm.graphReport = c3.generate({
                      bindto: '#jbl-graph-report',
                      data: {
                          x: 'x',
                          columns: graph,
                          type: 'bar',
                          //labels: {
                          //    format: function(v) {
                          //        return v + ' Minutes';
                          //    }
                          //},
                          groups: [activityNames],
                          colors: (function(){
                              return vm.getColorCodes(graph);
                          })()
                      },
                      tooltip: {
                          format: {
                              name: function (name) {
                                  return name.charAt(0).toUpperCase() + name.slice(1);
                              },
                              value: function (x) {
                                  return x + ' Minutes';
                              }
                          }
                      },
                      legend: {
                          show: false
                      },
                      axis : {
                          rotated: true,
                          x: {
                              type: 'category',
                              label: { text: 'Student Name', position: 'outer-top' }
                          },
                          y: {
                              label: { text: 'Time in Minutes', position: 'outer-center' }
                          }
                      },
                      bar: { width: 20 },
                      grid: {y: { show: true } }
                  });
                  $timeout(function() {
                      vm.graphReport.resize({height: 100 * vm.selected.users.length});
                      vm.createGraphControls(graph, vm.graphReport, {
                          timespend: {
                              axis: 'y'
                          }
                      });
                  }, 10);
              }, 0);
          });
      };

      /**
       * @description : Grade & Engagement Graph
       */
      vm.createGradeEngagementGraph = function(data) {
          vm.getGraphRecords(data, {timespend: true, grade: true, sort: true}, function (graph) {
              var activityNames = [];
              angular.forEach(vm.engagements, function(en) {
                  if (en.selected === true) {
                      activityNames.push(en.name);
                  }
              });

              $timeout(function() {
                  vm.showGraph = true;
                  vm.showTable = false;
                  vm.graphDataReady = true;
                  vm.graphReport = c3.generate({
                      bindto: '#jbl-graph-report',
                      data: {
                          x: 'x',
                          columns: graph,
                          type: 'bar',
                          //labels: {
                          //    format: function (v, id) {
                          //        return (id === 'Grade') ? v : v + ' Minutes';
                          //    }
                          //},
                          groups: [activityNames],
                          axes: {
                              'Grade': 'y2'
                          },
                          colors: (function(){
                              return vm.getColorCodes(graph);
                          })()
                      },
                      tooltip: {
                          format: {
                              name: function (name) {
                                  return name.charAt(0).toUpperCase() + name.slice(1);
                              },
                              value: function (x, ratio, id) {
                                  return (id === 'Grade') ? x : x + ' Minutes';
                              }
                          }
                      },
                      legend: {
                          show: false
                      },
                      axis : {
                          rotated: true,
                          x: {
                              type: 'category',
                              label: { text: 'Student Name', position: 'outer-top' }
                          },
                          y: {
                              label: { text: 'Time in Minutes', position: 'outer-center' }
                          },
                          y2: {
                              show: true,
                              label: { text: 'Grade', position: 'outer-center' }
                          }
                      },
                      bar: { width: 20 },
                      grid: {y: { show: true } }
                  });
                  $timeout(function() {
                      vm.graphReport.resize({height: 100 * vm.selected.users.length});
                      vm.createGraphControls(graph, vm.graphReport, {
                          grade: {
                              axis: 'y2'
                          },
                          timespend: {
                              axis: 'y'
                          }
                      });
                  }, 10);
              }, 100);
          });
      };

      /**
       * @description: creating crushed data for graph from raw data
       */
      vm.getGraphRecords = function(data, options, callback) {
          var temp = [],
              activityNames = [];

          angular.forEach(vm.engagements, function(en) {
              if (en.selected === true) {
                  activityNames.push(en.id);
              }
          });
          angular.forEach(data, function(d) {
              var obj = {};
              obj.x = d.studentName;
              obj.gradeObj = {};
              angular.forEach(vm.engagements, function(en) {
                  if (en.selected === true) {
                      obj[en.id] = 0;
                  }
              });
              angular.forEach(d.grade, function(rec) {
                  if (activityNames.indexOf(rec.quizType) > -1) {
                      if (angular.isUndefined(obj[rec.quizType])) {
                          obj[rec.quizType] = 0;
                      }
                      obj[rec.quizType] += parseInt(rec.timespend);
                  }
                  if (rec.quizType !== 'ebook') {
                      if (angular.isUndefined(obj.gradeObj[rec.quizType])) {
                          obj.gradeObj[rec.quizType] = {
                              grade: 0,
                              gradeCount: 0
                          };
                      }
                      if (rec.grade !== null) {
                          obj.gradeObj[rec.quizType].grade += parseInt(rec.grade);
                          obj.gradeObj[rec.quizType].gradeCount++;
                      }
                  }
              });
              /**
               * obj.gradeObj = {
               *   quiz1: {grade: 0, count: 0},
               *   quiz2: {grade: n, count: m},
               *   quiz3: {grade: l, count: k}
               * };
               *
               * after below statement execution
               *
               * gradeObj = {
               *   grade: ((n/m) + (l/k) ),
               *   count: 2
               * };
               *
               * final
               *
               * grade = gradeObj.grade / gradeObj.count
               */
              if (options.grade === true) {
                  var gradeObj = {
                      grade: 0,
                      gradeCount: 0
                  };
                  angular.forEach(obj.gradeObj, function(g) {
                      if (g.grade !== 0) {
                          gradeObj.grade += parseFloat(g.grade / g.gradeCount);
                      }
                      gradeObj.gradeCount++;
                  });
                  obj.avg = (gradeObj.gradeCount > 0) ? (gradeObj.grade / gradeObj.gradeCount).toFixed(2) : 0;
              }
              delete obj.gradeObj;
              temp.push(obj);
          });

          if (options.sort === true && options.grade === true) {
              temp = $filter('orderBy')(temp, 'grade');
          }

          var graph = {x: ['x']};
          if (options.grade === true) {
              graph.avg =  ['Grade'];
          }

          if (options.timespend === true) {
              angular.forEach(vm.engagements, function(en) {
                  if (en.selected === true) {
                      graph[en.id] = [en.name];
                  }
              });
          }
          angular.forEach(temp, function(t, i) {
              angular.forEach(t, function(rec, key) {
                  if (angular.isDefined(graph[key])) {
                      graph[key][i+1] = rec;
                      //(key !== 'x' && key !== 'avg') ? vm.timespendFormat(rec) : rec;
                  }
              });
          });

          vm.graphData = vm.toArray(graph);
          //table data
          $timeout(function() {
              vm.avgGrade = null;
              vm.avgTimespend = null;
              var sumGrade = 0,
                  countGrade = 0,
                  sumTime = 0,
                  countTime = 0,
                  grade = false,
                  timespend = false;
              var studArray = [];
              angular.forEach(graph, function(data) {
                  if (data[0] === 'x') {
                      angular.forEach(data, function(d, k) {
                          if (k > 0) {
                              studArray[k] = {
                                  timespend: 0
                                  //, timeCount: 0
                              };
                          }
                      });
                  }
              });

              angular.forEach(graph, function(data) {
                  if (data[0] === 'Grade') {
                      angular.forEach(data, function(val, id) {
                          if (id > 0) {
                              sumGrade += parseFloat(val);
                              countGrade++;
                              grade = true;
                          }
                      });
                  } else {
                      if (data[0] !== 'x' && (angular.isDefined(vm.toggledIds[data[0]]) && vm.toggledIds[data[0]] === true)) {
                          angular.forEach(data, function(val, id) {
                              if (id > 0 && parseFloat(val) > 0) {
                                  studArray[id].timespend += parseFloat(val);
                                  //studArray[id].timeCount++;
                                  timespend = true;
                              }
                          });
                      }
                  }
              });

              if (grade === true) {
                  vm.avgGrade = Math.round(sumGrade/countGrade);
              }
              if (timespend === true) {
                  // vm.avgTimespend = Math.round((sumTime/countTime) / 60);
                  angular.forEach(studArray, function(data) {
                      sumTime += data.timespend || 0; //(data.timeCount > 0) ? parseFloat(data.timespend/data.timeCount) : 0;
                      countTime++;
                  });
                  vm.avgTimespend = Math.round(sumTime/countTime);
              }
          }, 1000);

          $timeout(function () {
              callback(vm.toArray(graph));
          }, 0);
      };

      vm.createGraphControls = function (graph, chart, avg) {
          vm.sortBy = null;
          var idArr = [];
          angular.forEach(vm.toggledIds, function(val, key) {
              vm.toggledIds[key] = false;
          });
          angular.forEach(graph, function(data) {
              if (data[0] !== 'x') {
                  idArr.push(data[0]);
              }
              vm.toggledIds[data[0]] = true;
          });
          d3.select('.c3-controller').remove();
          (function(that) {
              d3.select('.graph-controls')
                  .insert('ul', '.chart')
                  .attr('class', 'list-unstyled list-inline text-capitalize c3-controller')
                  .selectAll('li')
                  .data(idArr)
                  .enter().append('li').attr('class', 'graphctrl text on')
                  .attr('data-id', function (id) { return id; })
                  .html(function (id) { return '<span class="type">'+ id + '</span>'; })
                  .each(function (id) {
                      var vm = this;
                      d3.select(vm).select('span')
                          .style('border-bottom-color', chart.color(id));
                  })
                  .on('mouseover', function (id) {
                      var vm = this,
                          elem = d3.select(vm);
                      if (elem.attr('class').replace(/[\n\t]/g, ' ').indexOf('on') > 0) {
                          chart.focus(id);
                      }
                  })
                  .on('mouseout', function () {
                      chart.revert();
                  })
                  .on('click', function (id) {
                      var vm = this;
                      var elem = d3.select(vm);
                      that.toggledIds[id] = !(elem.attr('class').replace(/[\n\t]/g, ' ').indexOf('on') > 0);
                      if (that.toggledIds[id] === false) {
                          elem.attr('class', 'graphctrl text off');
                      } else {
                          elem.attr('class', 'graphctrl text on');
                      }
                      that.createGraphAverageLine(graph, chart, avg);
                      if (that.sortBy !== null) {
                          that.sortGraph(function(){
                              $timeout(function(){
                                  chart.toggle(id);
                              }, 1300);
                          });
                      } else {
                          $timeout(function(){
                              chart.toggle(id);
                          }, 1300);
                      }
                  });
          })(vm);
          d3.select('.show-labels').on('click', function () {
              d3.selectAll('.graphctrl').attr('class', 'graphctrl text on');
              chart.show(idArr);
          });
          d3.select('.hide-labels').on('click', function () {
              d3.selectAll('.graphctrl').attr('class', 'graphctrl text off');
              chart.hide(idArr);
          });

          vm.createGraphAverageLine(graph, chart, avg);
      };

      vm.avgValueLastState = false;
      vm.createGraphAverageLine = function(graph, chart, avg) {
          var sumGrade = 0,
              countGrade = 0,
              sumTime = 0,
              countTime = 0,
              grade = false,
              timespend = false;
          var studArray = [];
          angular.forEach(graph, function(data) {
              if (data[0] === 'x') {
                  angular.forEach(data, function(d, k) {
                      if (k > 0) {
                          studArray[k] = {
                              timespend: 0
                              //,timeCount: 0
                          };
                      }
                  });
              }
          });

          angular.forEach(graph, function(data) {
              if (data[0] === 'Grade') {
                  angular.forEach(data, function(val, id) {
                      if (id > 0) {
                          sumGrade += parseFloat(val);
                          countGrade++;
                          grade = true;
                      }
                  });
              } else {
                  if (data[0] !== 'x' && (angular.isDefined(vm.toggledIds[data[0]]) && vm.toggledIds[data[0]] === true)) {
                          angular.forEach(data, function(val, id) {
                              if (id > 0 && parseFloat(val) > 0) {
                                  studArray[id].timespend += parseFloat(val);
                                  //studArray[id].timeCount++;
                                  timespend = true;
                              }
                          });
                  }
              }
          });
          if (grade === true) {
              var avgGrade = (sumGrade/countGrade).toFixed(2);
          }
          if (timespend === true) {
              angular.forEach(studArray, function(data) {
                  sumTime += data.timespend || 0; //(data.timeCount > 0) ? parseFloat(data.timespend/data.timeCount) : 0;
                  countTime++;
              });
              var avgTimespend = (sumTime/countTime).toFixed(2);
          }
          // Adding Average Line Controls
          (function(that) {
              chart.ygrids.remove();
              d3.select('.avgctrl').remove();
              d3.select('.c3-controller')
                  .append('li').attr('class', 'avgctrl text off')
                  .html('<span class="type" style="border-bottom-color: '+that.getColorCodes('average')+'">Average</span>')
                  .on('click', null)
                  .on('click', function () {
                      var vm = this,
                          elem = d3.select(vm);
                      if (elem.attr('class').replace(/[\n\t]/g, ' ').indexOf('on') > 0) {
                          chart.ygrids.remove();
                          elem.attr('class', 'avgctrl text off');
                      } else {
                          if (angular.isDefined(avg.grade) && that.toggledIds['Grade'] === true) {
                              chart.ygrids.add({value: avgGrade, axis: avg.grade.axis, text: 'Grade'});
                          }
                          if (angular.isDefined(avg.timespend)) {
                              chart.ygrids.add({value: avgTimespend, axis: avg.timespend.axis, text: 'Time'});
                          }
                          elem.attr('class', 'avgctrl text on');
                      }
                      that.avgValueLastState = !that.avgValueLastState;
                  });
              $timeout(function() {
                  if (that.avgValueLastState === true) {
                      if (that.toggledIds['Grade'] === true) {
                          chart.ygrids.add({value: avgGrade, axis: avg.grade.axis, text: 'Grade'});
                      } else {
                          chart.ygrids.remove({value: avgGrade});
                      }
                      /** @TODO check if all engagements turned off */
                      if (angular.isDefined(avg.timespend)) {
                          chart.ygrids.add({value: avgTimespend, axis: avg.timespend.axis, text: 'Time'});
                      }
                      d3.select('.avgctrl').attr('class', 'avgctrl text on');
                  } else {
                      chart.ygrids.remove();
                  }
              }, 500);
          })(vm);
      };
      /**
       * Sorting By selected type
       */
      vm.sortGraph = function(callback) {
          var orig = vm.toObject(angular.copy(vm.graphData));
          var data = vm.toObject(angular.copy(vm.graphData));
          //name sorting
          if (vm.sortBy === 'name') {
              var orderName = [];
              angular.forEach(data, function(d, id) {
                  if (id === 'x') {
                      orderName = angular.copy(d).sort(function(a, b) {
                          var left = a.toLowerCase(), right = b.toLowerCase();
                          if (left < right) {
                              return -1;
                          }
                          if (left > right) {
                              return  1;
                          }
                          return  0;
                      });
                  }
              });
              angular.forEach(orig, function(o, key) {
                  if (key !== 'x') {
                      angular.forEach(orderName, function(val, index) {
                          data[key][index] = orig[key][orig['x'].indexOf(val)];
                      });
                  }
              });
              data['x'] = orderName;
          }
          // grade sorting
          if (vm.sortBy === 'grade') {
              var orderGrade = [];
              angular.forEach(data, function(d, id) {
                  if (id === 'Grade') {
                      orderGrade = vm.sortWithIndeces(angular.copy(d)).sortIndices;
                  }
              });
              angular.forEach(orig, function(o, key) {
                  angular.forEach(orderGrade, function(val, index) {
                      data[key][index] = orig[key][val];
                  });
              });
          }

          // engagement time sorting
          if (vm.sortBy === 'engagement') {
              var sumTime = [];
              angular.forEach(data, function(d, id) {
                  if (id !== 'Grade' && id !== 'x' && (angular.isDefined(vm.toggledIds[id]) && vm.toggledIds[id] === true)) {
                      angular.forEach(d, function(val, key) {
                          if (angular.isUndefined(sumTime[key])) {
                              sumTime[key] = 0;
                          }
                          sumTime[key] += parseFloat(val);
                      });
                  }
              });
              var orderTime = vm.sortWithIndeces(angular.copy(sumTime)).sortIndices;
              angular.forEach(orig, function(o, key) {
                  angular.forEach(orderTime, function(val, index) {
                      data[key][index] = orig[key][val];
                  });
              });
          }

          // refreshing graph
          $timeout(function() {
              vm.graphReport.load({
                  columns: vm.toGraphArray(data)
              });
              if (angular.isDefined(callback) && angular.isFunction(callback)) {
                  callback();
              }
          }, 10);
      };

      vm.toGraphArray = function(data) {
          var temp = [];
          angular.forEach(data, function(d, id) {
              temp.push([id].concat(d));
          });
          return temp;
      };
      vm.toObject = function (data) {
          var temp = {};
          angular.forEach(data, function(d) {
              temp[d[0]] = d.slice(1, d.length);
          });
          return temp;
      };
      vm.objlen = function (data) {
          var keys = Object.keys(data);
          return keys.length;
      };

      vm.orderKeys = function(obj) {
          var keys = Object.keys(obj).sort(function keyOrder(k1, k2) {
              if (k1 < k2) return -1;
              else if (k1 > k2) return +1;
              else return 0;
          });

          var i, after = {};
          for (i = 0; i < keys.length; i++) {
              after[keys[i]] = obj[keys[i]];
              delete obj[keys[i]];
          }

          for (i = 0; i < keys.length; i++) {
              obj[keys[i]] = after[keys[i]];
          }
          return obj;
      };
      /**
       * @description : Getting selected chapters list / engagement list
       */
      vm.getString = function (type) {
          var str = (angular.isDefined(type) && angular.isDefined(vm.stepsStr[type])) ? vm.stepsStr[type] : null;
          if (str !== null && (type === 'top' || type === 'bottom')) {
              str += ' ' + vm.percent[type] + '%';
          }
          return str;
      };

      /**
       * @description : Export to CSV
       */
      vm.exportToCSV = function() {
          var csvContent = "";
          var avgArr = [[], []];
          if (vm.avgGrade !== null) {
              avgArr[0].push('Average Grade ( % )');
              avgArr[1].push(vm.avgGrade);
          }
          if (vm.avgTimespend !== null) {
              avgArr[0].push('Average Timespent ( Minutes )');
              avgArr[1].push(vm.avgTimespend);
          }

          csvContent += "\n";
          angular.forEach(avgArr, function(data, index) {
              var dataString = data.join(",");
              csvContent += index < avgArr.length ? dataString + "\n" : dataString;
          });
          csvContent += "\n";

          // header 1
          csvContent += "Student,";
          angular.forEach(vm.tableHeaders.chapters, function(data){
             csvContent += data.key;
             csvContent += new Array(data.span + 1).join(',');
          });
          // header 2
          if (vm.by.step3 !== null) {
              csvContent += "\n";
              csvContent += " ,";
              angular.forEach(vm.tableHeaders.assessments, function(data){
                  csvContent += data.key;
                  csvContent += new Array(data.span + 1).join(',');
              });
          }
          // header 3
          csvContent += "\n";
          csvContent += " ,";
          csvContent += vm.tableHeaders.studentHeaders.join(',');
          // body
          angular.forEach(vm.tableHeaders.values, function(data, key){
              csvContent += "\n";
              csvContent += key +',';
              csvContent += data.join(',');
          });

          if (navigator.msSaveBlob){
              var blob = new Blob([csvContent],{type: "text/csv;charset=utf-8;"});
              navigator.msSaveBlob(blob, "csvname.csv");
              return false;
          }

          var encodedUri = encodeURI("data:text/csv;charset=utf-8,"+csvContent),
              link = $document[0].createElement('a'),
              datetime = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString() ;
          link.setAttribute('id', 'csv-export');
          link.setAttribute('href', encodedUri);
          link.setAttribute('download', 'Graph Report - '+datetime+' .csv');
          $document[0].body.appendChild(link);
          $timeout(function() {
              link.click();
              link.remove();
          }, 200);
      };

      /**
       * initiating getting courses list and engagements list at startup
       */
      vm.getActivities();
  }
})();
